sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function (Controller, MessageToast) {
	"use strict";

	return Controller.extend("com.demo.s276.ex03.EXFIORI_03.controller.Add", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Add
		 */
		//	onInit: function() {
		//
		//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Add
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Add
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Add
		 */
		//	onExit: function() {
		//
		//	}
		
		onSave: function(){
			// get input value
			var inpSO = this.byId("inpSO").getValue();
			var inpDate = this.byId("inpDate").getValue() + "T00:00:00";
			var inpPerson = this.byId("inpPerson").getValue();
			var inpRef = this.byId("inpRef").getValue();
			
			// create default properties
			var oProperties = {
				Zvblen: inpSO,
				Zerdat: inpDate,
				Zernam: inpPerson,
				Zbstnk: inpRef
			};
	
			// create new entry in the model
			this._oContext = this.getView().getModel().createEntry("/DemoTableSet", { properties: oProperties,
														success: this._onCreateSuccess.bind(this),
														error: this._onCreateError.bind(this)});
			this.getView().getModel().submitChanges();
		},
		
		_onCreateSuccess: function(o){
			var router = sap.ui.core.UIComponent.getRouterFor(this);
			router.navTo("worklist", {}, true);
	
			// show success messge
			MessageToast.show(o.Zvblen + " 新增成功", {closeOnBrowserNavigation: false});
		},
		
		_onCreateError: function(o){
			this.getView().getModel().deleteCreatedEntry(this._oContext);
		}

	});

});
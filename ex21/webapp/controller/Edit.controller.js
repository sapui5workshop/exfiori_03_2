sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function (Controller, MessageToast) {
	"use strict";

	return Controller.extend("com.demo.s276.ex03.EXFIORI_03.controller.Edit", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Edit
		 */
			onInit: function() {
				var router = sap.ui.core.UIComponent.getRouterFor(this);
				router.getRoute("Edit").attachPatternMatched(this._onRouteMatched, this);
			},
			
			_onRouteMatched: function(oEvent){
				var sObjectId = oEvent.getParameter("arguments").objectId;
				
				var oModel = this.getView().getModel();
				this._sPath = "/" + oModel.createKey("DemoTableSet", {
				 		Zvblen : sObjectId
				 	});
					
				var inpSO = sObjectId;
				var inpDate = oModel.getProperty( this._sPath + "/Zerdat");
				var inpPerson = oModel.getProperty(this._sPath + "/Zernam");
				var inpRef = oModel.getProperty(this._sPath + "/Zbstnk");
				
				// convert date format
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({pattern : "yyyy-MM-dd"});   
				var dateFormatted = dateFormat.format(inpDate);

				this.byId("inpSO").setValue(inpSO);
				this.byId("inpDate").setValue(dateFormatted);
				this.byId("inpPerson").setValue(inpPerson);
				this.byId("inpRef").setValue(inpRef);
			},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Edit
		 */
		//	onBeforeRendering: function() {
		//
		//	}

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Edit
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf com.demo.s276.ex03.EXFIORI_03.view.Edit
		 */
		//	onExit: function() {
		//
		//	}
		
		onEdit: function(){
			// get input value
			var inpSO = this.byId("inpSO").getValue();
			var inpDate = this.byId("inpDate").getValue() + "T00:00:00";
			var inpPerson = this.byId("inpPerson").getValue();
			var inpRef = this.byId("inpRef").getValue();
			
			// create default properties
			var oProperties = {
				Zvblen: inpSO,
				Zerdat: inpDate,
				Zernam: inpPerson,
				Zbstnk: inpRef
			};
			
			this.getView().getModel().update(this._sPath, oProperties, {
				    success: this._onUpdateSuccess.bind(this),
					error: this._onUpdateError.bind(this)
			});	
		},
		
		_onUpdateSuccess: function(o){
			var router = sap.ui.core.UIComponent.getRouterFor(this);
			router.navTo("worklist", {}, true);
			MessageToast.show("更新成功", {closeOnBrowserNavigation: false});
		},
		
		_onUpdateError: function(o){
			MessageToast.show("更新失敗");
		}

	});

});